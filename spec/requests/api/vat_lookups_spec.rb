require 'swagger_helper'

RSpec.describe 'api/vat_lookups', type: :request do

  path '/api/validate/vat' do

    get('validate vat_lookup') do

      produces 'application/json'

      parameter name: 'code', :in => :query, type: :string, description: 'potential VAT number ex. ATU99999999'

      response(200, 'successful') do
        let(:code) { 'ATU99999999' }

        run_test!
      end

      response '422', 'invalid request' do
        before do
          allow_any_instance_of(CloudmersiveValidateApiClient::VatLookupRequest).to receive(:vat_code).and_return(nil)
        end
        let(:code) { 'unknown' }

        run_test!
      end
    end
  end
end
