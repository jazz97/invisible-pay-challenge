require 'swagger_helper'

RSpec.describe 'api/currencies', type: :request do

  path '/api/currency/convert' do

    get('convert currency') do
      produces 'application/json'

      parameter name: 'target', :in => :query, type: :string, required: true, description: 'target currency ex. EUR, INR, USD'
      parameter name: 'amount', :in => :query, type: :string, required: true, description: 'amount (in source-currency) ex 100, 3600'
      parameter name: 'source', :in => :query, type: :string, required: true, description: 'source currency ex. EUR, INR, USD'

      response(200, 'successful') do
        let(:source) { 'USD' }
        let(:target) { 'INR' }
        let(:amount) { '1000' }

        run_test!
      end

      response '422', 'invalid request' do
        let(:source) { 'unknown' }
        let(:target) { 'INR' }
        let(:amount) { '1000' }

        run_test!
      end
    end
  end

  path '/api/timestamp' do

    get('timestamp currency') do
      response(200, 'successful') do

        after do |example|
          example.metadata[:response][:examples] = { 'application/json' => JSON.parse(response.body, symbolize_names: true) }
        end
        run_test!
      end
    end
  end
end
