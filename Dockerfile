# vim:set ft=dockerfile:
FROM ruby:2.6-alpine

LABEL maintainer="Jaswinder <jaswindersinghsn97+1@gmail.com>"

ENV NOKOGIRI_USE_SYSTEM_LIBRARIES=1

RUN apk update \
  && apk add ruby \
             ruby-bigdecimal \
             ruby-bundler \
             ruby-io-console \
             ruby-irb \
             ca-certificates \
             libressl \
             less \
  && apk add --virtual build-dependencies \
             build-base \
             ruby-dev \
             gcc \
             wget \
             git \
             libressl-dev \
             libxml2-dev \
             libxslt-dev \
             pcre-dev \
             libffi-dev \
             libxml2 \
             postgresql-dev \
             tzdata

# set an environment variable where the Rails app is installed inside of Docker Image
ENV RAILS_ROOT /var/www/invisible-pay-service
ENV BUNDLE_PATH /bundles
RUN mkdir -p $RAILS_ROOT
RUN mkdir -p $BUNDLE_PATH

# Set working directory, where the commands will be ran:
WORKDIR $RAILS_ROOT

# Adding gems
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock

# Adding project files
COPY . .

EXPOSE 3000
