Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
    get '/currency/convert', to: 'currencies#convert'
    get '/timestamp', to: 'currencies#timestamp'
    get '/validate/vat', to: 'vat_lookups#validate'
  end
end
