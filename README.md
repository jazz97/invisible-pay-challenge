# README

This is a Micro-Service Application built with Rails API only framework skipping ActiveRecord

## Steps are necessary to get the application up and running.

### Ruby version - ruby 2.6.5

### System dependencies
  * Redis
  * Docker version 17.05.0-ce
  * Docker Compose version 1.12.0

### Configuration

### Database initialization
  No database required

### How to run the test suite
  - `rspec`

  Note: In order to add more specs, use `rails generate rspec:swagger your-controller-class-name-here`

  for ex.  `rails generate rspec:swagger Api::VatLookupsController`

### Services (cache servers etc.)
  * Use redis instance for cache store ( `redis://redis-cache-01:6379/0`)

  Custom external HTTP interactions:
  * CurrencyLayer public API using service class (`CurrencyLayer::ConversionService`)
  * Vat Lookups Cloudmersive Validate API using a gem `cloudmersive-validate-api-client`

### Deployment instructions
  1. Add required RAILS_MASTER_KEY env to master-key.env Or export to terminal session `export RAILS_MASTER_KEY= your key here`
  1. run docker compose services using `docker-compose up -d`

  Note: To stop services, use `docker-compose down`

### Secrets
  Verify API keys and other secrets encrypted in `credentials.yml.enc` file by opening in an editor:
  EDITOR="nano" rails credentials:edit

  This will show content in decrepted format using RAILS_MASTER_KEY

### API documentation using Swagger UI OpenAPI format
  - `/api-docs`

####  How to keep document updated using swagger automatically?

   `rake rswag:specs:swaggerize`

### More suggestions
  - It would be better to implement circuit breaker at application level also apart from API gateway level. This would resilient external calls to Cloudmersive and CurrencyLayer API services incase of their API service is down
