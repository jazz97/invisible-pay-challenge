# frozen_string_literal: true

class Api::CurrenciesController < ApplicationController
  before_action :quote_amount, only: [:convert]

  def convert
    render json: { amount: @quoted_amount }, status: 200
  end

  def timestamp
    render json: { timestamp: Time.now }, status: 200
  end

  private

  def conversion_params
    params.permit(:amount, :source, :target)
  end

  def quote_amount
    @quoted_amount = Rails.cache.fetch("quoted-amount-#{conversion_params[:target]}-#{conversion_params[:source]}",
                                       expires_in: 100.seconds) do
      Rails.logger.info '-- creating new cache key --'
      CurrencyLayer::ConversionService.new(target: { currency: conversion_params[:target] },
                                           source: {
                                             currency: conversion_params[:source],
                                             amount: conversion_params[:amount]
                                           }).run
    end
  rescue Exception => e
    render(json: { errors: e.message }, status: 422) && (return)
  end
end
