# frozen_string_literal: true

class Api::VatLookupsController < ApplicationController
  before_action :set_vat_lookup_instance, only: [:validate]

  def validate
    render json: { country_code: @vat_lookup.country_code }, status: 200
  end

  private

  def code_params
    params.permit(:code)
  end

  def set_vat_lookup_instance
    api_instance = CloudmersiveValidateApiClient::VatApi.new
    input = CloudmersiveValidateApiClient::VatLookupRequest.new
    input.vat_code = code_params[:code]
    @vat_lookup = Rails.cache.fetch("vat-lookup-#{input.vat_code}", expires_in: 100.seconds) do
      api_instance.vat_vat_lookup(input)
    end
    unless @vat_lookup.country_code
      raise CloudmersiveValidateApiClient::ApiError
    end
  rescue CloudmersiveValidateApiClient::ApiError => e
    render(json: { errors: e.message }, status: 422) && (return)
  end
end
