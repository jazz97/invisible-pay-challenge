# frozen_string_literal: true

require 'httparty'

class CurrencyLayer::ConversionService
  BASE_URL = 'http://apilayer.net/api/live'
  ACCESS_KEY = Rails.application.credentials.currency_layer_access_key
  DEFAULT_CURRENCY = 'USD'
  TIMEOUT = 5

  def initialize(source: {}, target: {})
    @source_currency = source[:currency] || DEFAULT_CURRENCY
    @source_amount = source[:amount].to_f

    @target_currency = target[:currency] || DEFAULT_CURRENCY
    @target_amount = 0.0
  end

  def run
    args = {
      access_key: ACCESS_KEY,
      source: @source_currency,
      currencies: @target_currency,
      format: 1
    }
    Rails.logger.info "exchange args: #{args[:source]} to #{args[:currencies]}"
    response = HTTParty.send(:get,
                             BASE_URL,
                             query: args,
                             headers: {},
                             verify: false,
                             timeout: TIMEOUT)
    if response.parsed_response['success']
      return @target_amount = @source_amount * conversion_quote(response)
    else
      return handle_error(response)
    end
  end

  private

  def handle_error(response)
    raise response['error']['info']
  end

  def conversion_quote(response)
    response.parsed_response['quotes'].values.first
  end
end
